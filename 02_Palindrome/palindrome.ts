function checkPalindrome(inputString: string): boolean
{
    // Check the lenght of input string 
    if (inputString.length < 1 || inputString.length > Math.pow(10, 5))
        return false;

    // Set the value to lowercase
    inputString = inputString.toLowerCase();

    // Convert the string to array
    const tmpArray = inputString.split('');

    // Reverse the array
    const revseArray = tmpArray.reverse();

    // Covert reverse array back to string
    const revseInputString = revseArray.join('');

    // Compare the value
    if (inputString == revseInputString)
        return true;
    else
        return false;
}

// ---------------------------------------------------------------- //
// Define and initialize variables
const a = "aabaa";
const b = "tnfodxxzqtivgnostongvitqzxxdofnt";
const c = "";

// Display result
console.log(checkPalindrome(a));
console.log(checkPalindrome(b));
console.log(checkPalindrome(c));
// ---------------------------------------------------------------- //