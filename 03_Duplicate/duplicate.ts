function firstDuplicate(a: number[]): number 
{
    // Check the lenght of input arrary 
    if (a.length < 1 || a.length > Math.pow(10, 5))
        return -1;
    
    for (let i = 0; i < a.length; i++) 
    {
        // Initialize the value
        let value = Math.abs(a[i]);
        
        // console.log("index: ", i, " value: ", value);

        // Check the value was less than zero
        if (a[value - 1] < 0) 
            return value;

        // Assign value to negative value
        a[value - 1] = -a[value - 1];

        // console.log("a[value - 1]: ",  a[value - 1]);
      }
      return -1;
}

// ---------------------------------------------------------------- //
// Define and initialize variables
const a = [8, 1, 4, 8, 10, 1, 5, 7, 8, 7];
const b = [28, 19, 13, 6, 34, 48, 50, 3, 47, 18, 15, 34, 16, 11, 13, 3, 2, 4, 46, 6, 7, 3, 18, 9, 32, 21, 3, 21, 50, 10, 45, 13, 22, 1, 27, 18, 3, 27, 30, 44, 12, 30, 40, 1, 1, 31, 6, 18, 33, 5];
const c = [];

// Display result
console.log(firstDuplicate(a));
console.log(firstDuplicate(b));
console.log(firstDuplicate(c));
// ---------------------------------------------------------------- //