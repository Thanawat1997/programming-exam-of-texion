function centuryFromYear(year) {
    // Check the range of value
    if (year >= 1 && year <= 2005) {
        // Action with 2 digits on the front from 4 digits
        var prefixYear = Math.floor(year / 100);
        // Action with 2 digits on the back from 4 digits
        var suffixYear = (year % 100);
        // Formular and return process
        return prefixYear + (suffixYear > 0 ? 1 : 0);
    }
    else {
        return 0;
        // console.log("The range of year is invaild!");
    }
}
// ---------------------------------------------------------------- //
// Define and initialize variables
var a = 1905;
var b = 1700;
var c = 2021;
// Display result
console.log(centuryFromYear(a));
console.log(centuryFromYear(b));
console.log(centuryFromYear(c));
// ---------------------------------------------------------------- //
