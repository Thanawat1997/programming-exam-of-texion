function centuryFromYear(year: number): number
{
    // Check the range of value
    if (year >= 1 && year <= 2005)
    {
        // Action with 2 digits on the front from 4 digits
        let prefixYear = Math.floor(year / 100);

        // Action with 2 digits on the back from 4 digits
        let suffixYear = (year % 100);

        // Formular and return process
        return prefixYear + (suffixYear > 0 ? 1 : 0);
    }
    else
    {
        return 0;
        // console.log("The range of year is invaild!");
    }
}

// ---------------------------------------------------------------- //
// Define and initialize variables
const a = 1905;
const b = 1700;
const c = 2021;

// Display result
console.log(centuryFromYear(a));
console.log(centuryFromYear(b));
console.log(centuryFromYear(c));
// ---------------------------------------------------------------- //